(function(){

    var Rate = angular.module("Rate",[]);

    //defining a service 
    var RateSvc = function($http){
        var rateSvc = this;

    
        rateSvc.getRate = function(rateParam){

            console.log("Base: %s and symbols : %s",rateParam.base , rateParam.symbols);

            return ($http.get("http://api.fixer.io/latest" , {
				params: rateParam
			}) );
        }
    }

        
    
    //creating services 
    Rate.service("RateSvc",["$http",RateSvc]);
})();