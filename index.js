//controller for index.js 

(function(){
    //connecting to dependency
    var RateApp = angular.module("RateApp",["Rate"]);

    var initRates = function(ctrl){
        ctrl.base = "";
        ctrl.symbols = "";
        ctrl.rates = [];
    }

    var checkAndGetParam = function(ctrl){
        
        if( ctrl.base !==  "" && ctrl.symbols !==  "") {
           return { base:ctrl.base, symbols:ctrl.symbols};
        } 
        else if( ctrl.base !==  "") {
           return { base:ctrl.base};
        }
        else if( ctrl.symbols !==  ""){
            return { symbols:ctrl.symbols};
        }
        return {};
    }

    //adding RateSvc denpendency
    var RateCtrl = function(RateSvc){
        var rateCtrl = this;
        
        initRates(rateCtrl); 
        
        rateCtrl.getRate = function(){

        RateSvc.getRate(checkAndGetParam(rateCtrl))
                .then(function(result){ 
                        //console.log(JSON.stringify(result.data.rates));
                        console.log(result.data.rates);
                        rateCtrl.rates = result.data.rates;
                    })
                .catch(function(status){
                        rateCtrl.rates = "something went wrong";
                    })
        }
       

		initRates(rateCtrl);
    }

        //GET /latest?base&symbols=HKD HTTP/1.1
        //GET/ latest?base=INR&symbols=USD
        //GET/latest?symbols=INR
        //GET/latest?base&symbols=INR
        //GET/latest
       /* rateCtrl.getRate = function(){

            console.log("Base: %s and symbols : %s",rateCtrl.base , rateCtrl.symbols);

	        $http
                .get("http://api.fixer.io/latest" ,{
				params: checkAndGetParam(rateCtrl)
                })
                .then(function(result){ 
                        //console.log(JSON.stringify(result.data.rates));
                        console.log(result.data.rates);
                        rateCtrl.rates = result.data.rates;
                    })
                .catch(function(status){
                        rateCtrl.rates = "something went wrong";
                    });
		initRates(rateCtrl);
        }*/

    
//adding RateSvc denpendency 
RateApp.controller("RateCtrl",["RateSvc",RateCtrl]);
})();